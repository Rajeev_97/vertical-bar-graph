
    public with sharing class BarGraphController {

       public List<BarGraphWrapper> listBarGraphWrapper {get;set;}
        public String renderAs { get; set; }


       public PageReference onInit() {
        renderAs='pdf';
        Double totalSum=20+22+22+22;
        listBarGraphWrapper.add(BarGraphController.caluculateGraphValue(totalSum, 20, 'Beginning Value','#808080'));
        listBarGraphWrapper.add(BarGraphController.caluculateGraphValue(totalSum, 22, 'Cash In','green'));
        listBarGraphWrapper.add(BarGraphController.caluculateGraphValue(totalSum, 22, 'Total Market Value Change','green'));
        listBarGraphWrapper.add(BarGraphController.caluculateGraphValue(totalSum, 22, 'Cash Out','red'));

        }
        public static BarGraphWrapper caluculateGraphValue(Double totalSum,Double value,String name,String colour){
            BarGraphWrapper warpper=new BarGraphWrapper();
            warpper.name=name;
            warpper.value=value;
            warpper.percentage=Integer.valueOf((value/totalSum)*100);
            warpper.colour=colour;
            return warpper;

        }
    }
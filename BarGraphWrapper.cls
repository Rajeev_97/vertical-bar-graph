public with sharing class BarGraphWrapper {
    @AuraEnabled public String name {get; set;}
    @AuraEnabled public Double value {get; set;}
    @AuraEnabled public Integer percentage {get; set;}
    @AuraEnabled public String colour {get; set;}

    public BarGraphWrapper(){
        name='';
        value=0.00;
        percentage=0;
        colour='';
    }
}   